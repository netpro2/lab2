import socket

HOST = 'localhost'
PORT = 22

def main():
    server_socket = socket.socket(socket.AF_INET, socket.SOCK_STREAM)
    server_socket.bind((HOST, PORT))
    server_socket.listen(1)
    print(f"Server listening on {HOST}:{PORT}")
    

    while True:
        client_socket, client_address = server_socket.accept()
        print(f"Connection from {client_address}")

        with open(client_socket.recv(1024), 'r') as file:
            content = file.read()

        client_socket.send(content.encode())
        client_socket.close()
        print("Data sent to client.")

if __name__ == "__main__":
    main()