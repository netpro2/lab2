import socket

HOST = 'localhost' 
PORT = 22

def main():
    client_socket = socket.socket(socket.AF_INET, socket.SOCK_STREAM)
    client_socket.connect((HOST, PORT))
    client_socket.sendall(b"a.txt")

    buffer_size = 1024
    data = b""

    while True:
        chunk = client_socket.recv(buffer_size)
        if not chunk:
            break
        data += chunk

    print("Received data:")
    print(data.decode())

    client_socket.close()

if __name__ == "__main__":
    main()